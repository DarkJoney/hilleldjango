
from django.core.management.base import BaseCommand

from group.models import Group
import random


class Command(BaseCommand):
    help = 'Generates phone number if it is empty'

    def handle(self, *args, **options):
        groups = Group.objects.all()
        for group in groups:
            group.name = f'Group - {random.choice(range(5))}'
            group.course = random.choice([
                "IT 210: Web Application Development.",
                "IT 226: Enterprise Information Systems.",
                "IT 227: E-Commerce Technologies.",
                "IT 238: Networking and Client/Server Computing.",
                "IT 280: Internet Security.",
                "IT 295: IT-Based Application Project.",
                "IT 299: Graduate Seminar.",
            ])
            group.starting_date = Group.starting_date.field.default
            group.save()
        self.stdout.write('SUCCESS')
