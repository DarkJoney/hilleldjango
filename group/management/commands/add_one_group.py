
from django.core.management.base import BaseCommand

from group.models import Group


class Command(BaseCommand):
    help = 'Generates phone number if it is empty'

    def handle(self, *args, **options):
        Group.generate_group()
        self.stdout.write('SUCCESS')
