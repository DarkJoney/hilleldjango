from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin

from group.models import Group
from group.forms import GroupForm

from student.utils import format_list


def get_groups_old(request):
    all_entries = list(Group.objects.values_list().order_by('-id'))
    groupid = request.GET.get('group')
    if groupid:
        all_entries = list(Group.objects.values_list().filter(group_number=groupid))
    return render(
        request=request,
        template_name='show_groups.html',
        context={
            'groups': format_list(all_entries)
        }
    )


class GetGroupListView(LoginRequiredMixin, ListView):
    about_view = 'Shows the list of available groups.'
    model = Group
    template_name = 'show_groups.html'
    context_object_name = 'group'
    paginate_by = 10

    def get_queryset(self):
        all_entries = super().get_queryset().select_related('teacher').all()
        request = self.request
        group_number = request.GET.get('group_number')
        curator = request.GET.get('group_curator')
        students = request.GET.get('students')
        starting_date = request.GET.get('starting_date')

        if group_number:
            all_entries = all_entries.filter(group_number=group_number)
        if curator:
            all_entries = all_entries.filter(curator=curator)
        if students:
            all_entries = all_entries.filter(students=students)
        if starting_date:
            all_entries = all_entries.filter(starting_date=starting_date)
        return all_entries

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['groups'] = self.get_queryset()
        context['description'] = self.about_view
        return context


class GroupCreateView(LoginRequiredMixin, CreateView):
    about_view = 'Add a new group to the system'
    model = Group
    form_class = GroupForm
    template_name = 'create_group.html'
    success_url = reverse_lazy('group:allgroups')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['teachers'] = self.get_object().teacher.all()
        context['description'] = self.about_view
        return context


class GroupUpdateView(LoginRequiredMixin, UpdateView):
    about_view = 'Edit the existing group at the system.'
    model = Group
    form_class = GroupForm
    template_name = 'edit_group.html'
    context_object_name = 'group'
    success_url = reverse_lazy('group:allgroups')
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['teachers'] = self.get_object().teacher.all()
        context['group'] = self.model
        context['students'] = self.get_object().students.all()
        context['description'] = self.about_view
        return context


class GroupDeleteView(LoginRequiredMixin, DeleteView):
    model = Group
    success_url = reverse_lazy('group:allgroups')
    pk_url_kwarg = 'id'
