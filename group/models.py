from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.core.exceptions import ValidationError

import datetime
import random


class Classroom(models.Model):
    name = models.CharField(max_length=32)
    floor = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(2), MaxValueValidator(5)]
    )

    def __str__(self):
        return f'{self.name}, {self.floor}'


class Group(models.Model):
    name = models.CharField(max_length=64, default="")
    curator = models.CharField(max_length=64, null=False, default='Place curator here')
    course = models.CharField(max_length=64, null=False, default='No students')
    starting_date = models.DateTimeField(editable=True, blank=True, null=False, default=datetime.date.today())
    head = models.OneToOneField(
         to='student.Student',
         null=True,
         on_delete=models.SET_NULL,
         related_name='head_of_group'
     )
    teacher = models.ForeignKey(
        to='teacher.Teacher',
        null=True,
        on_delete=models.SET_NULL,
        related_name='groups'
    )
    classrooms = models.ManyToManyField(
        to=Classroom,
        related_name='groups'
    )

    def __str__(self):
        return f'{self.name} ({self.course})'

    @staticmethod
    def generate_group():
        group = Group(
            name=f'Group - {random.choice(range(5))}',
            course=random.choice([
                "IT 210: Web Application Development.",
                "IT 226: Enterprise Information Systems.",
                "IT 227: E-Commerce Technologies.",
                "IT 238: Networking and Client/Server Computing.",
                "IT 280: Internet Security.",
                "IT 295: IT-Based Application Project.",
                "IT 299: Graduate Seminar.",
            ])
        )
        group.save()

    def clean(self):
        from student.models import Student
        target_student = Student.objects.get(uuid=self.head.uuid)
        if self.pk is not target_student.group.id:
            raise ValidationError("You can't have a person not from this group as the head")

    def save(self, *args, **kwargs):
        self.clean()
        super().save(*args, **kwargs)
