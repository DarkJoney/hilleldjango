from django.urls import path
from group.views import GroupCreateView, GroupDeleteView, GetGroupListView, GroupUpdateView

app_name = 'group'

urlpatterns = [
    path('', GetGroupListView.as_view(), name='allgroups'),
    path('create/', GroupCreateView.as_view(), name='create'),
    path('edit/<pk>', GroupUpdateView.as_view(), name='edit'),
    path('delete/<int:pk>', GroupDeleteView.as_view(), name='delete'),
]
