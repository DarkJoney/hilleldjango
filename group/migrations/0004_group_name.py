# Generated by Django 3.1.1 on 2020-09-20 13:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('group', '0003_group_head'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='name',
            field=models.CharField(default='', max_length=64),
        ),
    ]
