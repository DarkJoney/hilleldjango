import random

from student.models import Student


def gen_password(length):
    result = ''.join([
        str(random.randint(0, 9))
        for _ in range(length)
    ])
    return result


def parse_length(request, default=10):
    length = request.GET.get('length', str(default))
    if not length.isnumeric():
        raise ValueError("VALUE ERROR: int")
    length = int(length)
    print(length)
    if not 1 < length < 101:
        raise ValueError("RANGE ERROR: [3..10]")
    return length


def parse_count(request, default=10):
    count = request.GET.get('count', str(default))
    if not count.isnumeric():
        raise ValueError("VALUE ERROR: int")
    length = int(count)
    print(length)
    if not 1 <= length <= 100:
        raise ValueError("RANGE ERROR: [1..100]")
    return length


def create_students(length):
    for i in range(0, length):
        data_object = Student.generate_random_student()
        data_object.save()


def format_list(lst):
    return '<br>'.join(str(elem) for elem in lst)


def parse_name(request):
    name = request.GET.get('name')
    return name


def parse_lastname(request):
    last_name = request.GET.get('lastname')
    return last_name
