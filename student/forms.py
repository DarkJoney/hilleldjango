from django import forms
from django.core.exceptions import ValidationError

from string import ascii_letters, digits, punctuation, whitespace

from student.models import Student


class StudentCreateForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ['first_name', 'last_name', 'phone_number', 'birthdate', 'group']

    def check_phone_number(self):
        phone_num = self.cleaned_data['phone_number']

        allowed = '+-' + digits
        banned = (digits + ascii_letters + punctuation + whitespace) - allowed
        phone_num = str(phone_num)
        contains_digit = any(map(str.isdigit, phone_num))
        if not contains_digit:
            raise ValidationError('Phone number is wrong!')
        contains_banned = any(map(banned, phone_num))
        if contains_banned:
            raise ValidationError('Phone number is wrong!')
        return phone_num
