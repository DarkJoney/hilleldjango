from django.db import models

import datetime
import uuid

from faker import Faker

from core.models import Person


class Student(Person):
    id = models.CharField(primary_key=True, max_length=64, null=False)
    phone_number = models.CharField(max_length=84, null=False, default='+380-00-00-000')
    uuid = models.UUIDField(default=uuid.uuid4, editable=True)
    group = models.ForeignKey(
        to='group.Group',
        null=True,
        on_delete=models.SET_NULL,
        related_name='students',
        # editable=False
    )

    @classmethod
    def generate_random_student(cls):
        fake = Faker('en_US')
        student_object = Student(first_name=fake.first_name(),
                                 last_name=fake.last_name(), birthdate=fake.date())
        return student_object

    def full_name(self):
        return f'{self.first_name}, {self.last_name}'

    def get_phone(self):
        return f'{self.phone_number}'

    def age(self):
        return datetime.datetime.now().date().year - self.birthdate.year

    def __str__(self):
        return f'{self.id}, {self.full_name()}, {self.age()}'
