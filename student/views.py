from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin

from student.utils import create_students, parse_length, parse_count, gen_password, format_list
from student.models import Student
from student.forms import StudentCreateForm


def hello(request):
    return HttpResponse('Hello from Django!')


def get_random(request):
    try:
        length = parse_length(request, 10)
    except Exception as ex:
        return HttpResponse(str(ex), status_code=400)

    result = gen_password(length)

    return HttpResponse(result)


def generate_students(request):
    try:
        length = parse_count(request, 10)
    except Exception as ex:
        return HttpResponse(str(ex), status_code=400)
    create_students(length)
    all_entries = list(Student.objects.values_list().order_by('-id')[:length])
    return HttpResponse(format_list(all_entries))


class StudentListView(LoginRequiredMixin, ListView):
    about_view = 'View the available students of our facility.'
    model = Student
    template_name = 'students_view.html'
    context_object_name = 'student'
    paginate_by = 5
    ordering = ['id']

    def get_queryset(self):
        all_entries = super().get_queryset().all()
        request = self.request
        first_name = request.GET.get('first_name')
        last_name = request.GET.get('last_name')
        phone_number = request.GET.get('phone_number')
        birthday = request.GET.get('birthday')

        if first_name:
            all_entries = all_entries.filter(first_name=first_name)
        if last_name:
            all_entries = all_entries.filter(last_name=last_name)
        if phone_number:
            all_entries = all_entries.filter(phone_number=phone_number)
        if birthday:
            all_entries = all_entries.filter(birthday=birthday)
        return all_entries

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['students'] = self.get_queryset()
        context['description'] = self.about_view
        return context


class StudentCreateView(LoginRequiredMixin, CreateView):
    model = Student
    form_class = StudentCreateForm
    template_name = 'student_create.html'
    success_url = reverse_lazy('students:allstudent')


class StudentEditView(LoginRequiredMixin, UpdateView):
    model = Student
    form_class = StudentCreateForm
    template_name = 'student_edit.html'
    context_object_name = 'student'
    success_url = reverse_lazy('students:allstudent')
    pk_url_kwarg = 'uuid'
