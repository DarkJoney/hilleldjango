from django.core.management.base import BaseCommand

from faker import Faker

from student.models import Student


class Command(BaseCommand):
    help = 'Generates phone number if it is empty'

    def handle(self, *args, **options):
        fake = Faker('en_US')
        DEFAULT_PHONE_NUMBER = Student.phone_number.field.default
        students = Student.objects.filter(phone_number=DEFAULT_PHONE_NUMBER)
        for student in students.only('phone_number'):
            student.phone_number = fake.phone_number()
            student.save()
        self.stdout.write('Successfully updated phone numbers!')
