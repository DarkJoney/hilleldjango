
from django.core.management.base import BaseCommand

from student.models import Student
from group.models import Group
import random


class Command(BaseCommand):
    help = 'Generates phone number if it is empty'

    def handle(self, *args, **options):
        data = Student.objects.all()
        for i in data:
            i.group = random.choice(list(Group.objects.all()))
            i.save()
        self.stdout.write('SUCCESS')
