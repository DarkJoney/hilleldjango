from django.core.management.base import BaseCommand
import uuid

from student.models import Student


class Command(BaseCommand):
    help = 'Generates phone number if it is empty'

    def handle(self, *args, **options):
        for item in Student.objects.all():
            item.uuid = uuid.uuid4()
            item.save()
        self.stdout.write('Successfully updated UUID data!')
