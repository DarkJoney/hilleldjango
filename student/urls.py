
from django.urls import path

from student.views import StudentCreateView, StudentListView, StudentEditView, generate_students

app_name = 'students'

urlpatterns = [
    path('generate-students/', generate_students),
    path('', StudentListView.as_view(), name='allstudent'),
    path('create/', StudentCreateView.as_view(), name='create'),
    path('edit/<pk>', StudentEditView.as_view(), name='edit'),
]
