# Generated by Django 3.1.1 on 2020-09-14 20:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0005_auto_20200912_1757'),
    ]

    operations = [
        migrations.RenameField(
            model_name='student',
            old_name='ph_num',
            new_name='phone_number',
        ),
    ]
