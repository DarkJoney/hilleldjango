from django.shortcuts import render # noqa


# Create your views here.
def index(request):
    about_view = 'Welcome to LMS!'
    return render(
        request=request,
        template_name='index.html',
        context={
            'description': about_view
        }
    )
