import datetime

from django.db import models


class BaseModel(models.Model):
    class Meta:
        abstract = True

    create_date = models.DateTimeField(auto_now_add=True, null=True)
    write_date = models.DateTimeField(auto_now_add=True, null=True)

    def save(self, *args, **kwargs):
        self.write_date = datetime.datetime.now()
        super().save(*args, **kwargs)


class Person(BaseModel):
    class Meta:
        abstract = True

    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=64, null=False)
    birthdate = models.DateTimeField(null=True, default=datetime.datetime.today)
