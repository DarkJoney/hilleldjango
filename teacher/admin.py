from django.contrib import admin  # noqa

from group.models import Group
from teacher.models import Teacher


class GroupTable(admin.TabularInline):
    model = Group
    fields = ['name', 'course', 'head', 'classrooms']
    readonly_fields = fields
    show_change_link = True


class TeacherAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name']
    fields = ['first_name', 'last_name']
    inlines = [GroupTable]


admin.site.register(Teacher, TeacherAdmin)
