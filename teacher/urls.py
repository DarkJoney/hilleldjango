from django.urls import path

from teacher.views import TeacherCreateView, TeacherDeleteView, TeacherListView, TeacherUpdateView

app_name = 'teacher'

urlpatterns = [
    path('',  TeacherListView.as_view(), name='allTeacher'),
    path('create/', TeacherCreateView.as_view(), name='create'),
    path('edit/<int:id>', TeacherUpdateView.as_view(), name='edit'),
    path('delete/<int:id>', TeacherDeleteView.as_view(), name='delete'),

]
