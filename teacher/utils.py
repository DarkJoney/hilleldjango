from teacher.models import Teacher


def get_teacher_names():
    curators_names = Teacher.objects.all().values_list('first_name')
    curators_surnames = Teacher.objects.all().values_list('last_name')
    output = []
    for i in range(len(curators_names)):
        data = (str(curators_names[i]) + ' ' + str(curators_surnames[i]))
        data = ''.join(c for c in data if c not in "(),'")
        output.append(data)
    return output
