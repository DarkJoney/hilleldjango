from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin

from student.utils import format_list, parse_name, parse_lastname
from teacher.models import Teacher
from teacher.forms import TeacherCreateForm

# Create your views here.


def get_teachers_old(request):
    name = parse_name(request)
    all_entries = list(Teacher.objects.all().order_by('-id'))
    if name is not None:
        if name.isnumeric():
            raise ValueError("VALUE ERROR: int")
    lname = parse_lastname(request)
    if lname is not None:
        if lname.isnumeric():
            raise ValueError("VALUE ERROR: int")
    position = request.GET.get('position')
    if name:
        all_entries = list(Teacher.objects.all().filter(first_name=name))
    if lname:
        all_entries = list(Teacher.objects.all().filter(last_name=lname))
    if position:
        all_entries = list(Teacher.objects.all().filter(position=position))
    return render(
        request=request,
        template_name='show_teachers.html',
        context={
            'teachers': format_list(all_entries)
        }
    )


class TeacherListView(LoginRequiredMixin, ListView):
    about_view = 'Views the available teachers of our facility.'
    model = Teacher
    template_name = 'show_teachers.html'
    context_object_name = 'teachers'
    paginate_by = 10

    def get_queryset(self):
        all_entries = super().get_queryset().all()
        request = self.request
        first_name = request.GET.get('first_name')
        last_name = request.GET.get('last_name')
        position = request.GET.get('position')

        if first_name:
            all_entries = all_entries.filter(first_name=first_name)
        if last_name:
            all_entries = all_entries.filter(last_name=last_name)
        if position:
            all_entries = all_entries.filter(position=position)
        return all_entries

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['groups'] = self.get_queryset()
        context['description'] = self.about_view
        return context


class TeacherCreateView(LoginRequiredMixin, CreateView):
    about_view = 'Add the new teacher to the system.'
    model = Teacher
    form_class = TeacherCreateForm
    template_name = 'create_teacher.html'
    success_url = reverse_lazy('teacher:allTeacher')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['description'] = self.about_view
        return context


class TeacherUpdateView(LoginRequiredMixin, UpdateView):
    about_view = 'Edits the existing teacher in the system.'
    model = Teacher
    form_class = TeacherCreateForm
    template_name = 'edit_teacher.html'
    context_object_name = 'group'
    success_url = reverse_lazy('teacher:allTeacher')
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['teacher'] = self.get_object().teacher.all()
        context['groups'] = self.get_object().teacher.groups.all()
        context['description'] = self.about_view
        return context


class TeacherDeleteView(LoginRequiredMixin, DeleteView):
    model = Teacher
    success_url = reverse_lazy('teacher:allTeacher')
    pk_url_kwarg = 'id'
