from django.db import models
from faker import Faker

# Create your models here.


class Teacher(models.Model):
    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=84, null=False)
    position = models.CharField(max_length=64, null=False, default='School employee')

    @classmethod
    def generate_random_teacher(cls):
        fake = Faker('en_US')
        teacher_object = Teacher(first_name=str(fake.first_name()),
                                 last_name=str(fake.last_name()),
                                 position=str(fake.job()))
        return teacher_object.save()
