from django.core.management.base import BaseCommand
from teacher.models import Teacher


class Command(BaseCommand):
    help = 'Generates fake teachers at our database'

    def add_arguments(self, parser):
        parser.add_argument('amount', type=int)

    def handle(self, *args, **options):
        teach_count = options['amount']
        for i in range(teach_count):
            Teacher.generate_random_teacher()
        self.stdout.write('Successfully created {0} teachers!'.format(teach_count))
