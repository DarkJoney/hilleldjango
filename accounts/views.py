from accounts.forms import AccountCreateForm, AccountPasswordChangeForm, AccountUpdateForm, AccountProfileUpdateForm
from accounts.models import UserActions, Profile

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView
from django.views.generic.edit import ProcessFormView
from django.shortcuts import render
from django.http import HttpResponseRedirect


class AccountCreateView(CreateView):
    model = User
    template_name = 'register.html'
    form_class = AccountCreateForm
    success_url = reverse_lazy('accounts:profile')

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, 'Registration is successful!')
        return result


class AccountLoginView(LoginView):
    template_name = 'login.html'

    def get_redirect_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('core:index')

    def form_valid(self, form):

        result = super().form_valid(form)
        user = self.request.user
        try:
            profile = self.request.user.profile
        except Exception as ex: # noqa
            profile = Profile.objects.create(user=self.request.user)
            profile.save()

        login_action = UserActions.USER_ACTION.LOGIN
        logged_user = UserActions.objects.create(user=user, action=login_action)
        messages.success(self.request, f'User {self.request.user} has been successfully logged in!')
        logged_user.save()
        return result


class AccountLogoutView(LogoutView):
    template_name = 'logout.html'

    def get_next_page(self):
        result = super().get_next_page()
        messages.success(self.request, 'you have successfully logged out')
        return result


class AccountUpdateView(LoginRequiredMixin, ProcessFormView):

    def get(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(instance=user)
        profile_form = AccountProfileUpdateForm(instance=profile)

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form,
            }
        )

    def post(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile
        user_form = AccountUpdateForm(data=request.POST, instance=user)
        profile_form = AccountProfileUpdateForm(data=request.POST, files=request.FILES, instance=profile)

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()

            return HttpResponseRedirect(reverse('accounts:profile'))

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form,
            }
        )


class AccountPasswordChangeView(PasswordChangeView):
    template_name = 'change_password.html'
    form_class = AccountPasswordChangeForm
    success_url = reverse_lazy('accounts:profile')

    def form_valid(self, form):
        result = super().form_valid(form)
        user = self.request.user
        login_action = UserActions.USER_ACTION.CHANGE_PASSWORD
        logged_user = UserActions.objects.create(user=user, action=login_action)
        logged_user.save()
        messages.success(self.request, 'Your password is successfully updated!')
        return result
