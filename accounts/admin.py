from accounts.models import Profile, UserActions

from django.contrib import admin # noqa

# Register your models here.


class ProfileAdmin(admin.ModelAdmin):
    fields = ('user', 'image')
    list_display = ('user', 'image')


class UserActionAdmin(admin.ModelAdmin):
    fields = ('user', 'action')
    readonly_fields = ('write_date',)
    list_display = ('user', 'write_date', 'action')


admin.site.register(Profile, ProfileAdmin)
admin.site.register(UserActions, UserActionAdmin)
